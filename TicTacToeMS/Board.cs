﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TicTacToeMS
{
    class Board
    {
        public static string[] CoOrdinates;

        public static string[] Abc =
        {
            "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
            "V", "W", "X", "Y", "Z"
        };

        public static Dictionary<string, int> AxisDictionary = new Dictionary<string, int>();

        public void InitializeCoOrdinates(int size)
        {
            CoOrdinates = new string[size * size];

            for (int i = 0; i < size * size; i++)
            {
                CoOrdinates[i] = " ";
            }
        }

        public void DrawGenericBoard(int size)
        {
            var upperBorder = string.Concat(Enumerable.Repeat(" _____", size));
            var middle = string.Concat(Enumerable.Repeat("     |", size - 1));
            var middleWithBase = string.Concat(Enumerable.Repeat("_____|", size - 1));
            Console.WriteLine($" {upperBorder}");

            for (int i = 1; i <= size; i++)
            {
                Console.WriteLine(" |     |" + middle);
                Console.Write($"{size - i}|  {CoOrdinates[size - i]}  |");

                if (!AxisDictionary.ContainsKey(Abc[0] + (size - i)))
                {
                    AxisDictionary.Add(Abc[0] + (size - i), size - i);
                }

                MiddleWithCoOrd(size, i);
                Console.WriteLine();
                Console.WriteLine(" |_____|" + middleWithBase);
            }
            DrawXaxisBase(size);
            Console.WriteLine();
        }

        private void MiddleWithCoOrd(int size, int i)
        {
            for (int j = 2; j < size + 1; j++)
            {
                Console.Write($"  {CoOrdinates[size * j - i]}  |");

                if (!AxisDictionary.ContainsKey(Abc[j - 1] + (size - i)))
                {
                    AxisDictionary.Add(Abc[j - 1] + (size - i), size * j - i);
                }
            }
        }

        private void DrawXaxisBase(int size)
        {
            for (int i = 0; i < size; i++)
            {
                Console.Write($"    {Abc[i]} ");
            }
        }
    }
}
