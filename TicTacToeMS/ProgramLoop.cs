﻿using System;

namespace TicTacToeMS
{
    class ProgramLoop
    {
        private bool _exit;
        private int _turnNo;

        public void Run()
        {
            Console.Clear();
            ShowMenu();
            var command = Console.ReadLine();

            while (!_exit)
            {
                switch (command)
                {
                    case "1":
                        GenericSizeGameForTwo(3);
                        break;
                    case "2":
                        Console.Clear();
                        ShowSizeMenu();
                        var commandSize = Console.ReadLine();
                        switch (commandSize)
                        {
                            case "1":
                                GenericSizeGameForTwo(4);
                                break;
                            case "2":
                                GenericSizeGameForTwo(5);
                                break;
                            case "3":
                                GenericSizeGameForTwo(6);
                                break;
                            case "4":
                                GenericSizeGameForTwo(7);
                                break;
                            case "5":
                                GenericSizeGameForTwo(8);
                                break;
                            case "6":
                                GenericSizeGameForTwo(9);
                                break;
                            case "7":
                                GenericSizeGameForTwo(10);
                                break;
                            case "Q":
                                Run();
                                break;
                            default:
                                Console.WriteLine("Command not found. Try again...");
                                break;
                        }
                        break;
                    case "3":
                        SinglePlayer(3);
                        break;
                    case "Q":
                        _exit = true;
                        break;
                    default:
                        Console.WriteLine("Command not found. Try again...");
                        break;
                }
            }
        }

        private void SinglePlayer(int size)
        {
            _turnNo = 0;
            var board = new Board();
            var winningStreak = 3;
            Board.AxisDictionary.Clear();
            board.InitializeCoOrdinates(size);
            Console.WriteLine("Player 1 -------- X\nPlayer 2 (COM) -- O\nPress any key to start...");
            Console.ReadKey();
            do
            {
                _turnNo++;
                Console.Clear();
                board.DrawGenericBoard(size);
                PlayerControl.SinglePlayerTurn(_turnNo);
            } while (!CheckForWin(winningStreak, size));

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
            Run();
        }

        private void ShowSizeMenu()
        {
            Console.WriteLine("Choose board size:");
            Console.WriteLine("<1> 4x4");
            Console.WriteLine("<2> 5x5");
            Console.WriteLine("<3> 6x6");
            Console.WriteLine("<4> 7x7");
            Console.WriteLine("<5> 8x8");
            Console.WriteLine("<6> 9x9");
            Console.WriteLine("<7> 10x10");
            Console.WriteLine("<Q> Back to main menu");
        }

        private void ShowMenu()
        {
            Console.WriteLine("<1> Classic Tic-Tac-Toe game for two");
            Console.WriteLine("<2> Generic-size Tic-Tac-Toe game for two");
            Console.WriteLine("<3> Player vs Com on classic-size board");
            Console.WriteLine("<Q> Quit");
        }

        private void GenericSizeGameForTwo(int size)
        {
            _turnNo = 0;
            int winningStreak;
            Board.AxisDictionary.Clear();

            if (size == 3)
            {
                winningStreak = 3;
            }
            else
            {
                winningStreak = PlayerControl.GetWinningStreak(size, "Provide winning streak:");
            }

            var board = new Board();
            board.InitializeCoOrdinates(size);
            Console.WriteLine("Player 1 --- X\nPlayer 2 --- O\nPress any key to start...");
            Console.ReadKey();
            do
            {
                _turnNo++;
                Console.Clear();
                board.DrawGenericBoard(size);
                PlayerControl.TwoPlayersTurn(_turnNo);
            } while (!CheckForWin(winningStreak, size));

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

        public bool CheckForWin(int winningStreak, int size)
        {
            if (WinCases.HorizontalWin(winningStreak, size) || WinCases.VerticalWin(winningStreak, size) 
                || WinCases.AcrossWinFromDownToLeft(winningStreak, size) || WinCases.AcrossWinFromDownToRight(winningStreak, size)
                || WinCases.AcrossWinFromUpToLeft(winningStreak, size) || WinCases.AcrossWinFromUpToRight(winningStreak, size))
            {
                Console.Clear();
                new Board().DrawGenericBoard(size);

                Console.WriteLine(_turnNo % 2 != 0 ? "--- Player 1 has won ---" : "--- Player 2 has won ---");
                return true;
            }
            
            if (WinCases.Tie(size))
            {
                Console.Clear();
                new Board().DrawGenericBoard(size);
                Console.WriteLine("--- Game ended with tie ---");
                return true;
            }
            return false;
        }
    }
}
