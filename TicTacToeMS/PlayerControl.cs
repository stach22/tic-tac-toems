﻿using System;
using System.Threading;

namespace TicTacToeMS
{
    public class PlayerControl
    {
        public static string Choice;
        public static string SymbolX = "X";
        public static string SymbolO = "O";

        public static string GetCoOrdinates()
        {
            string userInput = Console.ReadLine();
            var axisDict = Board.AxisDictionary;
            var coOrd = Board.CoOrdinates;

            while (string.IsNullOrEmpty(userInput) || !axisDict.ContainsKey(userInput) || coOrd[axisDict[userInput]] != " ")
            {
                Console.WriteLine("Wrong or already marked co-ordinates.\nProvide value using upper case letters, ex 'C2'");
                userInput = Console.ReadLine();
            }

            return userInput;
        }

        public static int GetWinningStreak(int size, string message)
        {
            Console.WriteLine(message);
            int number;

            while (!int.TryParse(Console.ReadLine(), out number) || number < 3 || number > size)
            {
                Console.WriteLine("Not a numeric value or input out of range (3 - board size) - try again...");
            }

            return number;
        }

        public static void TwoPlayersTurn(int turnNo)
        {
            if (turnNo % 2 != 0)
            {
                Console.WriteLine("Player 1 turn, provide co-ordinates...");
                PlaceSymbolOnBoard(GetCoOrdinates(), SymbolX);
            }
            else
            {
                Console.WriteLine("Player 2 turn, provide co-ordinates...");
                PlaceSymbolOnBoard(GetCoOrdinates(), SymbolO);
            }
        }

        public static void PlaceSymbolOnBoard(string coOrdinates, string symbol)
        {
            var index = Board.AxisDictionary[coOrdinates];
            Board.CoOrdinates[index] = symbol;
        }

        public static void SinglePlayerTurn(int turnNo)
        {
            if (turnNo % 2 != 0)
            {
                Console.WriteLine("Player 1 turn, provide co-ordinates...");
                PlaceSymbolOnBoard(GetCoOrdinates(), SymbolX);
            }
            else
            {
                Console.WriteLine("COM turn...");
                Thread.Sleep(1500);
                PlaceSymbolOnBoard(ComControl.ComChoose(), SymbolO);
            }
        }

    }
}
