﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TicTacToeMS
{
    class WinCases
    {
        public static bool HorizontalWin(int winningStreak, int size)
        {
            var coOr = Board.CoOrdinates;
            var axis = Board.AxisDictionary;

            var xRow = String.Concat(Enumerable.Repeat("X", winningStreak));
            var oRow = String.Concat(Enumerable.Repeat("O", winningStreak));

            for (int i = 0; i < size; i++)
            {
                var singleRow = new List<string>();
                var rowSelected = from k in axis.Keys where k.EndsWith($"{i}") select k;

                if (CheckWinningRow(rowSelected, axis, singleRow, coOr, xRow, oRow)) return true;

            }
            return false;
        }

        public static bool VerticalWin(int winningStreak, int size)
        {
            var coOr = Board.CoOrdinates;
            var axis = Board.AxisDictionary;
            var abc = Board.Abc;

            var xRow = String.Concat(Enumerable.Repeat("X", winningStreak));
            var oRow = String.Concat(Enumerable.Repeat("O", winningStreak));

            for (int i = 0; i < size; i++)
            {
                var singleRow = new List<string>();
                var rowSelected = from k in axis.Keys where k.StartsWith($"{abc[i]}") select k;

                if (CheckWinningRow(rowSelected, axis, singleRow, coOr, xRow, oRow)) return true;

            }
            return false;
        }

        public static bool AcrossWinFromDownToLeft(int winningStreak, int size)
        {
            var coOr = Board.CoOrdinates;
            var axis = Board.AxisDictionary;
            var abc = Board.Abc;

            var xRow = String.Concat(Enumerable.Repeat("X", winningStreak));
            var oRow = String.Concat(Enumerable.Repeat("O", winningStreak));

            for (int i = 0; i < size; i++)
            {
                var singleRow = new List<string>();
                var rowSelected = new List<string>();

                for (int j = 0; j < size; j++)
                {
                    var item = axis.Keys.SingleOrDefault(k => k == $"{abc[j]}{i + j}");
                    if (item != null)
                    {
                        rowSelected.Add(item);
                    }
                }

                if (CheckWinningRow(rowSelected, axis, singleRow, coOr, xRow, oRow)) return true;

            }

            return false;
        }

        public static bool AcrossWinFromDownToRight(int winningStreak, int size)
        {
            var coOr = Board.CoOrdinates;
            var axis = Board.AxisDictionary;
            var abc = Board.Abc;

            var xRow = String.Concat(Enumerable.Repeat("X", winningStreak));
            var oRow = String.Concat(Enumerable.Repeat("O", winningStreak));

            for (int i = 1; i < size; i++)
            {
                var singleRow = new List<string>();
                var rowSelected = new List<string>();

                for (int j = 0; j < size - 1; j++)
                {
                    var item = axis.Keys.SingleOrDefault(k => k == $"{abc[i + j]}{j}");
                    if (item != null)
                    {
                        rowSelected.Add(item);
                    }
                }

                if (CheckWinningRow(rowSelected, axis, singleRow, coOr, xRow, oRow)) return true;

            }

            return false;
        }

        public static bool AcrossWinFromUpToLeft(int winningStreak, int size)
        {
            var coOr = Board.CoOrdinates;
            var axis = Board.AxisDictionary;
            var abc = Board.Abc;

            var xRow = String.Concat(Enumerable.Repeat("X", winningStreak));
            var oRow = String.Concat(Enumerable.Repeat("O", winningStreak));

            for (int i = 1; i <= size; i++)
            {
                var singleRow = new List<string>();
                var rowSelected = new List<string>();

                for (int j = 0; j < size; j++)
                {
                    var item = axis.Keys.SingleOrDefault(k => k == $"{abc[j]}{size - j - i}");
                    if (item != null)
                    {
                        rowSelected.Add(item);
                    }
                }

                if (CheckWinningRow(rowSelected, axis, singleRow, coOr, xRow, oRow)) return true;

            }

            return false;
        }

        public static bool AcrossWinFromUpToRight(int winningStreak, int size)
        {
            var coOr = Board.CoOrdinates;
            var axis = Board.AxisDictionary;
            var abc = Board.Abc;

            var xRow = String.Concat(Enumerable.Repeat("X", winningStreak));
            var oRow = String.Concat(Enumerable.Repeat("O", winningStreak));

            for (int i = 0; i < size - 1; i++)
            {
                var singleRow = new List<string>();
                var rowSelected = new List<string>();

                for (int j = 0; j < size; j++)
                {
                    var item = axis.Keys.SingleOrDefault(k => k == $"{abc[j + i]}{size - j}");
                    if (item != null)
                    {
                        rowSelected.Add(item);
                    }
                }

                if (CheckWinningRow(rowSelected, axis, singleRow, coOr, xRow, oRow)) return true;

            }

            return false;
        }

        private static bool CheckWinningRow(IEnumerable<string> rowSelected, Dictionary<string, int> axis, List<string> singleRow, string[] coOr, string xRow,
            string oRow)
        {
            foreach (var field in rowSelected)
            {
                var value = axis[field];
                singleRow.Add(coOr[value]);
            }

            string xWinningRow = null;
            string oWinningRow = null;

            for (int j = 0; j < singleRow.Count; j++)
            {
                if (singleRow[j] == "X")
                {
                    xWinningRow += singleRow[j];
                }
                else
                {
                    xWinningRow = null;
                }

                if (singleRow[j] == "O")
                {
                    oWinningRow += singleRow[j];
                }
                else
                {
                    oWinningRow = null;
                }

                if (xWinningRow == xRow || oWinningRow == oRow)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool Tie(int size)
        {
            var coOr = Board.CoOrdinates;

            if (Array.TrueForAll(coOr, s => s == "X" || s == "O"))
            {
                return true;
            }

            return false;
        }
    }
}
