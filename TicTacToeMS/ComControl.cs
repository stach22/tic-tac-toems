﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TicTacToeMS
{
    class ComControl
    {
        // First, algorithm tries to win. If there's two O's or one empty field between two O's, algorithm tries to fill It. 
        // Second algorithm tries to block the opponent. If opponent has two symbols near each other or empty field
        // between two O's, algorithm will block It. In third way, it looking for any O already placed, and tries to place
        // another one near. If none of this option will work, algorithm is looking for all empty fields, and
        // places symbol random way.

        public static string ComChoose()
        {
            if (FillToWin() != null)
            {
                return FillToWin();
            }

            if (Block() != null)
            {
                return Block();
            }

            if (AddSecondToRow() != null)
            {
                return AddSecondToRow();
            }

            return PlaceOnRandomEmpty();
        }

        private static string AddSecondToRow()
        {
            var coOrd = Board.CoOrdinates;
            var axis = Board.AxisDictionary;

            var coor2Abc = coOrd[2] + coOrd[5] + coOrd[8];
            var coor1Abc = coOrd[1] + coOrd[4] + coOrd[7];
            var coor0Abc = coOrd[0] + coOrd[3] + coOrd[6];

            var coorA012 = coOrd[0] + coOrd[1] + coOrd[2];
            var coorB012 = coOrd[3] + coOrd[4] + coOrd[5];
            var coorC012 = coOrd[6] + coOrd[7] + coOrd[8];

            var coorAcross1 = coOrd[0] + coOrd[4] + coOrd[8];
            var coorAcross2 = coOrd[2] + coOrd[4] + coOrd[6];

            if (coor0Abc == "O  ")
            {
                return axis.FirstOrDefault(x => x.Value == 6).Key;
            }
            if (coor0Abc == "  O")
            {
                return axis.FirstOrDefault(x => x.Value == 3).Key;
            }
            if (coor0Abc == " O ")
            {
                return axis.FirstOrDefault(x => x.Value == 0).Key;
            }
            ////////////////////////////////
            if (coor1Abc == "O  ")
            {
                return axis.FirstOrDefault(x => x.Value == 4).Key;
            }
            if (coor1Abc == "  O")
            {
                return axis.FirstOrDefault(x => x.Value == 1).Key;
            }
            if (coor1Abc == " O ")
            {
                return axis.FirstOrDefault(x => x.Value == 7).Key;
            }
            ////////////////////////////////
            if (coor2Abc == "O  ")
            {
                return axis.FirstOrDefault(x => x.Value == 5).Key;
            }
            if (coor2Abc == "  O")
            {
                return axis.FirstOrDefault(x => x.Value == 2).Key;
            }
            if (coor2Abc == " O ")
            {
                return axis.FirstOrDefault(x => x.Value == 8).Key;
            }
            //////////////////
            if (coorA012 == "O  ")
            {
                return axis.FirstOrDefault(x => x.Value == 2).Key;
            }
            if (coorA012 == "  O")
            {
                return axis.FirstOrDefault(x => x.Value == 0).Key;
            }
            if (coorA012 == " O ")
            {
                return axis.FirstOrDefault(x => x.Value == 0).Key;
            }
            ///////////////////
            if (coorB012 == "O  ")
            {
                return axis.FirstOrDefault(x => x.Value == 5).Key;
            }
            if (coorB012 == "  O")
            {
                return axis.FirstOrDefault(x => x.Value == 4).Key;
            }
            if (coorB012 == " O ")
            {
                return axis.FirstOrDefault(x => x.Value == 3).Key;
            }
            //////////////////
            if (coorC012 == "O  ")
            {
                return axis.FirstOrDefault(x => x.Value == 7).Key;
            }
            if (coorC012 == "  O")
            {
                return axis.FirstOrDefault(x => x.Value == 6).Key;
            }
            if (coorC012 == " O ")
            {
                return axis.FirstOrDefault(x => x.Value == 8).Key;
            }
            ////////////////
            if (coorAcross1 == "O  ")
            {
                return axis.FirstOrDefault(x => x.Value == 4).Key;
            }
            if (coorAcross1 == "  O")
            {
                return axis.FirstOrDefault(x => x.Value == 0).Key;
            }
            if (coorAcross1 == " O ")
            {
                return axis.FirstOrDefault(x => x.Value == 8).Key;
            }
            //////////////////
            if (coorAcross2 == "O  ")
            {
                return axis.FirstOrDefault(x => x.Value == 4).Key;
            }
            if (coorAcross2 == "  O")
            {
                return axis.FirstOrDefault(x => x.Value == 2).Key;
            }
            if (coorAcross2 == " O ")
            {
                return axis.FirstOrDefault(x => x.Value == 6).Key;
            }
            return null;

        }

        private static string PlaceOnRandomEmpty()
        {
            var coOrd = Board.CoOrdinates;
            var axis = Board.AxisDictionary;
            var emptyCoOrds = new List<int>();

            for (int i = 0; i < coOrd.Length; i++)
            {
                if (coOrd[i] == " ")
                {
                    emptyCoOrds.Add(i);
                }
            }

            var r = new Random(DateTime.Now.Millisecond);
            var randomChoose = r.Next(emptyCoOrds.Count);

            return axis.FirstOrDefault(x => x.Value == randomChoose).Key;
        }

        private static string Block()
        {
            var coOrd = Board.CoOrdinates;
            var axis = Board.AxisDictionary;

            var coor2Abc = coOrd[2] + coOrd[5] + coOrd[8];
            var coor1Abc = coOrd[1] + coOrd[4] + coOrd[7];
            var coor0Abc = coOrd[0] + coOrd[3] + coOrd[6];

            var coorA012 = coOrd[0] + coOrd[1] + coOrd[2];
            var coorB012 = coOrd[3] + coOrd[4] + coOrd[5];
            var coorC012 = coOrd[6] + coOrd[7] + coOrd[8];

            var coorAcross1 = coOrd[0] + coOrd[4] + coOrd[8];
            var coorAcross2 = coOrd[2] + coOrd[4] + coOrd[6];

            if (coor0Abc == "XX ")
            {
                return axis.FirstOrDefault(x => x.Value == 6).Key;
            }
            if (coor0Abc == " XX")
            {
                return axis.FirstOrDefault(x => x.Value == 0).Key;
            }
            if (coor0Abc == "X X")
            {
                return axis.FirstOrDefault(x => x.Value == 3).Key;
            }
            ////////////////////////////////
            if (coor1Abc == "XX ")
            {
                return axis.FirstOrDefault(x => x.Value == 7).Key;
            }
            if (coor1Abc == " XX")
            {
                return axis.FirstOrDefault(x => x.Value == 1).Key;
            }
            if (coor1Abc == "X X")
            {
                return axis.FirstOrDefault(x => x.Value == 4).Key;
            }
            ////////////////////////////////
            if (coor2Abc == "XX ")
            {
                return axis.FirstOrDefault(x => x.Value == 8).Key;
            }
            if (coor2Abc == " XX")
            {
                return axis.FirstOrDefault(x => x.Value == 2).Key;
            }
            if (coor2Abc == "X X")
            {
                return axis.FirstOrDefault(x => x.Value == 5).Key;
            }
            //////////////////
            if (coorA012 == "XX ")
            {
                return axis.FirstOrDefault(x => x.Value == 2).Key;
            }
            if (coorA012 == " XX")
            {
                return axis.FirstOrDefault(x => x.Value == 0).Key;
            }
            if (coorA012 == "X X")
            {
                return axis.FirstOrDefault(x => x.Value == 1).Key;
            }
            ///////////////////
            if (coorB012 == "XX ")
            {
                return axis.FirstOrDefault(x => x.Value == 5).Key;
            }
            if (coorB012 == " XX")
            {
                return axis.FirstOrDefault(x => x.Value == 3).Key;
            }
            if (coorB012 == "X X")
            {
                return axis.FirstOrDefault(x => x.Value == 4).Key;
            }
            //////////////////
            if (coorC012 == "XX ")
            {
                return axis.FirstOrDefault(x => x.Value == 8).Key;
            }
            if (coorC012 == " XX")
            {
                return axis.FirstOrDefault(x => x.Value == 6).Key;
            }
            if (coorC012 == "X X")
            {
                return axis.FirstOrDefault(x => x.Value == 7).Key;
            }
            ////////////////
            if (coorAcross1 == " XX")
            {
                return axis.FirstOrDefault(x => x.Value == 0).Key;
            }
            if (coorAcross1 == "X X")
            {
                return axis.FirstOrDefault(x => x.Value == 4).Key;
            }
            if (coorAcross1 == "XX ")
            {
                return axis.FirstOrDefault(x => x.Value == 8).Key;
            }
            //////////////////
            if (coorAcross2 == " XX")
            {
                return axis.FirstOrDefault(x => x.Value == 2).Key;
            }
            if (coorAcross2 == "X X")
            {
                return axis.FirstOrDefault(x => x.Value == 4).Key;
            }
            if (coorAcross2 == "XX ")
            {
                return axis.FirstOrDefault(x => x.Value == 6).Key;
            }
            return null;
        }

        private static string FillToWin()
        {
            var coOrd = Board.CoOrdinates;
            var axis = Board.AxisDictionary;

            var coor2Abc = coOrd[2] + coOrd[5] + coOrd[8];
            var coor1Abc = coOrd[1] + coOrd[4] + coOrd[7];
            var coor0Abc = coOrd[0] + coOrd[3] + coOrd[6];

            var coorA012 = coOrd[0] + coOrd[1] + coOrd[2];
            var coorB012 = coOrd[3] + coOrd[4] + coOrd[5];
            var coorC012 = coOrd[6] + coOrd[7] + coOrd[8];

            var coorAcross1 = coOrd[0] + coOrd[4] + coOrd[8];
            var coorAcross2 = coOrd[2] + coOrd[4] + coOrd[6];

            if (coor0Abc == "OO ")
            {
                return axis.FirstOrDefault(x => x.Value == 6).Key;
            }
            if (coor0Abc == " OO")
            {
                return axis.FirstOrDefault(x => x.Value == 0).Key;
            }
            if (coor0Abc == "O O")
            {
                return axis.FirstOrDefault(x => x.Value == 3).Key;
            }
            ////////////////////////////////
            if (coor1Abc == "OO ")
            {
                return axis.FirstOrDefault(x => x.Value == 7).Key;
            }
            if (coor1Abc == " OO")
            {
                return axis.FirstOrDefault(x => x.Value == 1).Key;
            }
            if (coor1Abc == "O O")
            {
                return axis.FirstOrDefault(x => x.Value == 4).Key;
            }
            ////////////////////////////////
            if (coor2Abc == "OO ")
            {
                return axis.FirstOrDefault(x => x.Value == 8).Key;
            }
            if (coor2Abc == " OO")
            {
                return axis.FirstOrDefault(x => x.Value == 2).Key;
            }
            if (coor2Abc == "O O")
            {
                return axis.FirstOrDefault(x => x.Value == 5).Key;
            }
            //////////////////
            if (coorA012 == "OO ")
            {
                return axis.FirstOrDefault(x => x.Value == 2).Key;
            }
            if (coorA012 == " OO")
            {
                return axis.FirstOrDefault(x => x.Value == 0).Key;
            }
            if (coorA012 == "O O")
            {
                return axis.FirstOrDefault(x => x.Value == 1).Key;
            }
            ///////////////////
            if (coorB012 == "OO ")
            {
                return axis.FirstOrDefault(x => x.Value == 5).Key;
            }
            if (coorB012 == " OO")
            {
                return axis.FirstOrDefault(x => x.Value == 3).Key;
            }
            if (coorB012 == "O O")
            {
                return axis.FirstOrDefault(x => x.Value == 4).Key;
            }
            //////////////////
            if (coorC012 == "OO ")
            {
                return axis.FirstOrDefault(x => x.Value == 8).Key;
            }
            if (coorC012 == " OO")
            {
                return axis.FirstOrDefault(x => x.Value == 6).Key;
            }
            if (coorC012 == "O O")
            {
                return axis.FirstOrDefault(x => x.Value == 7).Key;
            }
            ////////////////
            if (coorAcross1 == " OO")
            {
                return axis.FirstOrDefault(x => x.Value == 0).Key;
            }
            if (coorAcross1 == "O O")
            {
                return axis.FirstOrDefault(x => x.Value == 4).Key;
            }
            if (coorAcross1 == "OO ")
            {
                return axis.FirstOrDefault(x => x.Value == 8).Key;
            }
            //////////////////
            if (coorAcross2 == " OO")
            {
                return axis.FirstOrDefault(x => x.Value == 2).Key;
            }
            if (coorAcross2 == "O O")
            {
                return axis.FirstOrDefault(x => x.Value == 4).Key;
            }
            if (coorAcross2 == "OO ")
            {
                return axis.FirstOrDefault(x => x.Value == 6).Key;
            }
            return null;

        }
    }
}
